exports.up = async function (sql) {
	await sql`
    CREATE TABLE IF NOT EXISTS patients (
        id SERIAL PRIMARY KEY NOT NULL,
        name CHARACTER VARYING(255) NOT NULL,
        sex CHARACTER VARYING(255) NOT NULL,
        department CHARACTER VARYING(255) NOT NULL,
        status CHARACTER VARYING(255) NOT NULL
    )
  `
}

exports.down = async function (sql) {
	await sql`
    DROP TABLE IF EXISTS patients
  `
}
