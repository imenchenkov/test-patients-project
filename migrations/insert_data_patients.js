exports.up = async function (sql) {
	await sql`
        INSERT INTO patients(name,sex,department,status)
        VALUES
            ('John', 'Male', 'Urology','Active'),
            ('Julia', 'Female', 'Cardiology','Not active'),
            ('Jess', 'Female', 'Therapeutic','Active'),
            ('Anna', 'Female', 'Gynecology','Not active'),
            ('Natasha', 'Female', 'Cardiology','Active')
  `
}

exports.down = async function (sql) {
	await sql`
        TRUNCATE TABLE  patients
  `
}
