import type { NextApiRequest, NextApiResponse } from 'next';
import { IPatients } from '../../lib/patients';
import { list } from '../../lib/patients';

export default async function handler(
	req: NextApiRequest,
	res: NextApiResponse<IPatients[] | string>
) {
	try {
		switch (req.method) {
			case 'GET':
				const patients = await list();
				return res.status(200).json(patients);
			default:
				return res.status(405).send('Method Not Allowed');
		}
	} catch (error) {
		return res.status(500).send('Internal Server Error');
	}
}
