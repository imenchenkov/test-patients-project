import React, { useEffect, useState } from 'react'
import { Layout } from '../components/Layout';
import { TableHeading } from '../components/TableHeading';
import { Patients } from '../components/Patients';
import { Dropdown } from 'flowbite-react';
import { IPatients } from '../lib/patients';
import LogInModal from '../components/LogInModal';

const MyApp = () => {
	const [patientsData, setPatientsData] = useState([]);
	const [tableHeaders, setTableHeaders] = useState([]);
	const [visible, setVisible] = useState(false);
	const [filter, setFilter] = useState('Department');
	const [activeTab, setActiveTab] = useState(true);

	const getPatients = async () => {
		try {
			const response = await fetch('api/patients');
			const data = await response.json();
			setPatientsData(data);
			setTableHeaders(Object.keys(data[0]));
		} catch (error) {
			console.error('Ошибка при получении данных', error);
		}
	};

	useEffect(() => {
		getPatients().then();
	}, []);

	const filteredData = filter && filter !== 'Department' ?
		patientsData.filter(item => item.department === filter)
		:
		patientsData
	;

	return (
		<Layout title='Patients'>
			<LogInModal
				show={visible}
				setShow={setVisible}
				activeTab={activeTab}
				setActiveTab={setActiveTab}
			/>
			{!!patientsData.length&&
				<div className='w-full flex justify-start mb-2 hidden lg:flex'>
					<Dropdown label={filter} dismissOnClick={false}>
						{patientsData.map((elem: IPatients) => (
							<Dropdown.Item
								key={elem.id}
								onClick={() => setFilter(elem.department)}
							>
								{elem.department}
							</Dropdown.Item>
						))}
					</Dropdown>
					<button
						className='group flex items-center justify-center p-0.5 text-center font-medium relative focus:z-10 focus:outline-none text-white bg-cyan-700 border border-transparent enabled:hover:bg-cyan-800 focus:ring-cyan-300 dark:bg-cyan-600 dark:enabled:hover:bg-cyan-700 dark:focus:ring-cyan-800 rounded-lg focus:ring-2 w-fit text-sm px-4 ml-5'
						onClick={() => setFilter('Department')}
					>
						Reset Filter
					</button>
					<button
						className='group flex items-center justify-center p-0.5 text-center font-medium relative focus:z-10 focus:outline-none text-white bg-cyan-700 border border-transparent enabled:hover:bg-cyan-800 focus:ring-cyan-300 dark:bg-cyan-600 dark:enabled:hover:bg-cyan-700 dark:focus:ring-cyan-800 rounded-lg focus:ring-2 w-fit text-sm px-4 ml-5'
						onClick={() => setVisible(true)}
					>
						Login
					</button>
				</div>
			}
			<table cellPadding="0" cellSpacing="0" className='table-fixed border-collapse text-left text-base w-full mt-5'>
				<thead className='bg-white'>
				<tr>
					{tableHeaders &&
						tableHeaders.map((header: string, index: number) => (
							<TableHeading key={index} heading={header} index={index} />
						))
					}
				</tr>
				</thead>
				<tbody className='bg-white'>
				{!!patientsData.length &&
					filteredData.map((patient: IPatients) => (
						<Patients
							key={patient.id}
							id={patient.id}
							name={patient.name}
							sex={patient.sex}
							department={patient.department}
							status={patient.status}
						/>
					))
				}
				</tbody>
			</table>
		</Layout>
	);
};

export default MyApp;
