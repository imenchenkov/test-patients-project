import sql from './db'

export interface IPatients {
	id: number,
	name: string,
	sex: string,
	department: string,
	status: string,
}

export async function list() {
	return sql<IPatients[]>`
		SELECT id, name, sex, department, status
		FROM patients
		ORDER BY id
	`;
}
