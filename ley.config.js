const dotenv = require('dotenv')
const { parse } = require('pg-connection-string')

dotenv.config({ path: '.env' })

const options = parse(process.env.DATABASE_URL || '')

module.exports = options
