import React, { FC } from 'react';
import Rodal from 'rodal';
import 'rodal/lib/rodal.css';

type TModal = {
	visible: boolean,
	setVisible: Function,
	closeOnEsc: boolean,
	showCloseButton: boolean,
	width?: number,
	height?: number,
};

const Modal: FC<TModal> = ({
	visible,
	setVisible,
	children,
	closeOnEsc = true,
	showCloseButton = false,
	width = 400,
	height = 240
}) => {
	return (
		<Rodal
			customStyles={{padding: 0}}
			showCloseButton={showCloseButton}
			closeOnEsc={closeOnEsc}
			visible={visible}
			onClose={setVisible}
			width={width}
			height={height}
		>
			{children}
		</Rodal>
	);
};

export default Modal;
