import React, { FC } from 'react';

type TTableHeading = {
	heading: string,
	index: number,
};

export const TableHeading: FC<TTableHeading> = ({heading, index}) => (
	<th
		className={`lg:table-cell ${index > 1 ? 'hidden' : ''} table-heading first:pl-6 px-4 last:pr-6 py-7 font-normal text-sch-primary  relative cursor-pointer relative before:bg-sch-blue-4 before:content-[''] before:absolute before:left-0 before:w-[0.063rem] first:before:content-none before:top-[1.75rem] before:h-[calc(100%-3.5rem)]`}
	>
		{heading.toUpperCase()}
	</th>
);
