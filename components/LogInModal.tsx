import React, { FC, useRef, useState } from 'react';
import Modal from '../components/Modal';
import Form from '../components/Form';

type TLogInModal = {
	show: boolean,
	setShow: Function,
	activeTab: boolean,
	setActiveTab: Function,
};

const LogInModal: FC<TLogInModal> = ({show, setShow, activeTab, setActiveTab }) => {
	const emailValidationRegExp = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
	const emailField = useRef(null);
	const passwordField = useRef(null);
	const [emailError, setEmailError] = useState('');
	const [passwordError, setPasswordError] = useState('');
	const [showPassword, setShowPassword] = useState(false);

	const submitForm = (event) => {
		if (!emailField.current.value) {
			setEmailError('Empty Field');
			event.preventDefault();
		} else if (!emailValidationRegExp.test(emailField.current.value)) {
			setEmailError('Incorrect Email');
			event.preventDefault();
		}

		if (!passwordField.current.value) {
			setPasswordError('Empty Field');
			event.preventDefault();
		} else if (passwordField.current.value.length < 6) {
			setPasswordError('Password Too Short');
			event.preventDefault();
		}

		
	};

	const checkIsEmpty = (field, method) => {
		field.current.value && method('');
	};

	const closeModal = () => {
		setShow(false);
		setEmailError('');
	};
	
	const togglePassword = () => {
		setShowPassword((prevState => !prevState));
	};

	return (
		<Modal visible={show} setVisible={closeModal} closeOnEsc={true} showCloseButton={false} height={320}>
			<div className='h-full flex flex-col gap-8'>
				<div className='flex justify-around text-center'>
					<div
						onClick={() => setActiveTab(true)}
						className={`w-full cursor-pointer h-14 flex justify-center items-center ${activeTab ? 'bg-white' : 'bg-slate-400'}`}
					>
						Log In
					</div>
					<div
						onClick={() => setActiveTab(false)}
						className={`w-full cursor-pointer h-14 flex justify-center items-center ${activeTab ? 'bg-slate-400' : 'bg-white'}`}
					>
						Sign Up
					</div>
				</div>
				<div>
					<Form submitFunc={(event) => submitForm(event)}>
						<div className={`relative mx-12 ${!emailError.length && 'mb-4'}`}>
							<div className="absolute inset-y-0 left-0 flex items-center pl-3.5 pointer-events-none">
								<svg
									className="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
									xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 20 16"
								>
									<path d="m10.036 8.278 9.258-7.79A1.979 1.979 0 0 0 18 0H2A1.987 1.987 0 0 0 .641.541l9.395 7.737Z"/>
									<path d="M11.241 9.817c-.36.275-.801.425-1.255.427-.428 0-.845-.138-1.187-.395L0 2.6V14a2 2 0 0 0 2 2h16a2 2 0 0 0 2-2V2.5l-8.759 7.317Z"/>
								</svg>
							</div>
							<input
								type="text"
								id="email"
								className={`${!!emailError.length ? 'focus:ring-red-600 border-red-600 focus:border-red-600 bg-red-200 placeholder-red-600 focus:shadow-red-600' : 'focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 bg-gray-50 border border-gray-300 text-gray-900'} text-sm rounded-lg block w-full pl-10 p-2.5`}
								placeholder="Email"
								ref={emailField}
								onChange={() => checkIsEmpty(emailField, setEmailError)}
							/>
						</div>
						{!!emailError.length &&
							<div className="mx-12 mb-4 text-red-600">
								{emailError}
							</div>
						}
						<div className={`relative mx-12 ${!passwordError.length && 'mb-4'}`}>
							<div className="absolute inset-y-0 left-0 flex items-center pl-3.5 pointer-events-none">
								<svg
									className="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true"
									xmlns="http://www.w3.org/2000/svg"
									viewBox="0 0 24 24"
								>
									<g>
										<path fill="none" d="M0 0h24v24H0z"/>
										<path d="M18 8h2a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h2V7a6 6 0 1 1 12 0v1zM5 10v10h14V10H5zm6 4h2v2h-2v-2zm-4 0h2v2H7v-2zm8 0h2v2h-2v-2zm1-6V7a4 4 0 1 0-8 0v1h8z"/>
									</g>
								</svg>
							</div>
							<input
								type={showPassword ? 'text' : 'password'}
								id="password"
								className={`${!!passwordError.length ? 'focus:ring-red-600 border-red-600 focus:border-red-600 bg-red-200 placeholder-red-600 focus:shadow-red-600' : 'focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 bg-gray-50 border border-gray-300 text-gray-900'} text-sm rounded-lg block w-full pl-10 p-2.5`}
								placeholder="•••••••••"
								onChange={() => checkIsEmpty(passwordField, setPasswordError)}
								ref={passwordField}
							/>
							<div className="absolute inset-y-0 right-0 flex items-center pr-3.5 cursor-pointer" onClick={togglePassword}>
								{showPassword ?
									<svg
										xmlns="http://www.w3.org/2000/svg"
										width="24"
										height="24"
										viewBox="0 0 24 24"
									>
										<path d="M19.604 2.562l-3.346 3.137c-1.27-.428-2.686-.699-4.243-.699-7.569 0-12.015 6.551-12.015 6.551s1.928 2.951 5.146 5.138l-2.911 2.909 1.414 1.414 17.37-17.035-1.415-1.415zm-6.016 5.779c-3.288-1.453-6.681 1.908-5.265 5.206l-1.726 1.707c-1.814-1.16-3.225-2.65-4.06-3.66 1.493-1.648 4.817-4.594 9.478-4.594.927 0 1.796.119 2.61.315l-1.037 1.026zm-2.883 7.431l5.09-4.993c1.017 3.111-2.003 6.067-5.09 4.993zm13.295-4.221s-4.252 7.449-11.985 7.449c-1.379 0-2.662-.291-3.851-.737l1.614-1.583c.715.193 1.458.32 2.237.32 4.791 0 8.104-3.527 9.504-5.364-.729-.822-1.956-1.99-3.587-2.952l1.489-1.46c2.982 1.9 4.579 4.327 4.579 4.327z"/>
									</svg>
									:
									<svg
										xmlns="http://www.w3.org/2000/svg"
										width="24"
										height="24"
										viewBox="0 0 24 24"
									>
										<path d="M15 12c0 1.654-1.346 3-3 3s-3-1.346-3-3 1.346-3 3-3 3 1.346 3 3zm9-.449s-4.252 8.449-11.985 8.449c-7.18 0-12.015-8.449-12.015-8.449s4.446-7.551 12.015-7.551c7.694 0 11.985 7.551 11.985 7.551zm-7 .449c0-2.757-2.243-5-5-5s-5 2.243-5 5 2.243 5 5 5 5-2.243 5-5z"/>
									</svg>
								}
							</div>
						</div>
						{!!passwordError.length &&
							<div className="mx-12 mb-4 text-red-600">
								{passwordError}
							</div>
						}
						<div className="flex justify-center">
							<input
								id="submit"
								className='w-9/12 cursor-pointer bg-cyan-700 enabled:hover:bg-cyan-800 focus:ring-cyan-300 dark:bg-cyan-600 dark:enabled:hover:bg-cyan-700 dark:focus:ring-cyan-800 text-white font-bold py-2 px-4 rounded'
								type="submit"
								value='Log In'
							/>
						</div>
					</Form>
				</div>
			</div>
		</Modal>
	);
};

export default LogInModal;
