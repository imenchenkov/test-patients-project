import React, { ReactNode, FC } from 'react';
import Head from 'next/head';

type TProps = {
	children?: ReactNode
	title?: string
}

export const Layout: FC<TProps> = ({ children, title = 'This is the default title' }) => (
	<div className='bg-[#C7DFF4]/20'>
		<Head>
			<title>{title}</title>
			<meta charSet="utf-8"/>
			<meta name="viewport" content="initial-scale=1.0, width=device-width"/>
		</Head>
		<div className='flex flex-col py-9 px-6 min-h-[calc(100vh-2.25rem)]'>
			<header>
				<h1 className='text-4xl font-bold leading-normal tracking-[0.01em]'>Patient list</h1>
				<hr className='my-6 -mx-6'/>
			</header>
			{children}
			<footer className='px-5 pt-5 text-xs text-center text-[#6D6E71]'>
				© Copyright
			</footer>
		</div>
	</div>
);
