import React, {FC, FormEvent} from 'react';

type TForm = {
	submitFunc: (event: FormEvent<HTMLFormElement>) => void,
};

const Form: FC<TForm> = ({children, submitFunc = () => {}}) => {
	return (
		<form onSubmit={submitFunc}>
			{children}
		</form>
	);
};

export default Form;
