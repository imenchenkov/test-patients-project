import React, { FC } from 'react';
import { IPatients } from '../lib/patients';

export const Patients: FC<IPatients> = (patient) => (
	<tr key={patient.id} className='border-[#f4f9fd] border-t-[0.75rem]'>
		<td className="first:pl-6 px-4 last:pr-6 py-6 relative before:bg-sch-blue-4 before:content-[''] before:absolute before:left-0 before:w-[0.063rem] first:before:content-none before:top-[1.5rem] before:h-[calc(100%-3rem)]">
			{patient.id}
		</td>
		<td className="first:pl-6 px-4 last:pr-6 py-6 relative before:bg-sch-blue-4 before:content-[''] before:absolute before:left-0 before:w-[0.063rem] first:before:content-none before:top-[1.5rem] before:h-[calc(100%-3rem)]">
			{patient.name}
		</td>
		<td className="first:pl-6 px-4 last:pr-6 py-6 relative before:bg-sch-blue-4 before:content-[''] before:absolute before:left-0 before:w-[0.063rem] first:before:content-none before:top-[1.5rem] before:h-[calc(100%-3rem)] hidden lg:table-cell">
			{patient.sex}
		</td>
		<td className="first:pl-6 px-4 last:pr-6 py-6 relative before:bg-sch-blue-4 before:content-[''] before:absolute before:left-0 before:w-[0.063rem] first:before:content-none before:top-[1.5rem] before:h-[calc(100%-3rem)] hidden lg:table-cell">
			{patient.department}
		</td>
		<td className="first:pl-6 px-4 last:pr-6 py-6 relative before:bg-sch-blue-4 before:content-[''] before:absolute before:left-0 before:w-[0.063rem] first:before:content-none before:top-[1.5rem] before:h-[calc(100%-3rem)] hidden lg:table-cell">
			{patient.status}
		</td>
	</tr>
);
